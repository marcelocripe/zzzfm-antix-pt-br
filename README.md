Repositórios oficiais do programa "zzzFM"

https://gitlab.com/skidoo/zzzfm
e
https://gitlab.com/antix-contribs/zzzfm



Traduções revisadas por marcelocripe:

https://gitlab.com/marcelocripe/zzzFM_pt_BR/-/blob/main/for_use_antix-contribs_zzzfmpot_pt_BR_12-02-2022.po?ref_type=heads

https://gitlab.com/marcelocripe/zzzFM_pt_BR/-/blob/main/zzzfm.desktop

https://gitlab.com/marcelocripe/zzzFM_pt_BR/-/blob/main/zzzfm-folder-handler.desktop

https://gitlab.com/marcelocripe/zzzFM_pt_BR/-/blob/main/zzzfm-find.desktop



Para utilizar o arquivo "for_use_antix-contribs_zzzfmpot_pt_BR_12-02-2022.po", "zzzfm.desktop", "zzzfm-folder-handler.desktop" e "zzzfm-find.desktop", inicie o Emulador de Terminal na pasta onde estão os arquivos que foram baixados e aplique os comandos.

"for_use_antix-contribs_zzzfmpot_pt_BR_12-02-2022.po":

Comando para converter o arquivo editável da tradução com a extensão ".po" para ".mo".

$ msgfmt for_use_antix-contribs_zzzfmpot_pt_BR_12-02-2022.po -o zzzfm.mo

Comando para copiar o arquivo da tradução com a extensão ".mo" para a pasta do idioma "pt_BR".

$ sudo cp zzzfm.mo /usr/share/locale/pt_BR/LC_MESSAGES


"zzzfm.desktop", "zzzfm-folder-handler.desktop" e "zzzfm-find.desktop":

Comando para copiar o arquivo com a extensão ".desktop" para a pasta /usr/share/applications.

$ sudo cp zzzfm.desktop /usr/share/applications

$ sudo cp zzzfm-folder-handler.desktop /usr/share/applications

$ sudo cp zzzfm-find.desktop /usr/share/applications

Comando para escrever globalmente todas as entradas dos menus do antiX:

$ sudo desktop-menu --write-out-global
